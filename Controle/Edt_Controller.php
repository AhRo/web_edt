<?php 

/**
 * Permet de choisir quelle fonction appeller avec la fonction Ajax
 */
if(!empty($_POST['action'])){
    $_POST['action']();
}

/**
 * Création initial de l'emploi du temps
 */
function init() {
    require("./Model/Edt_Model.php");
    $matieres = getMatieres();
    $modules = getModules();
    $periodes = getPeriodes();
    $periodesMatiere = getMatieresPeriod();
    $profsMatiere = getMatieresProf();
    $profs = getProfs();
	require ("./Vue/Edt.php");
}

/**
 * Permet d'enregistrer les modifications effectuer à l'emploi du temps.
 * Return nous à simplement permet de faire des tests
 */
function enregistrer(){
    require("../Model/Edt_Model.php");
    $idMatiere = $_POST['idMatiere'];
    $idPeriod = $_POST['idPeriod'];
    $nbH = $_POST['nbH'];
    enregistrerBDD($idMatiere,$idPeriod, $nbH);
    $tab = array();
    $tab[] = $idMatiere;
    $tab[] = $idPeriod;
    $tab[] = $nbH;
    echo json_encode($tab);
}

/**
 * Permet l'ajout d'une nouvelle matiere
 * Return des elements permettant l'ajout dynamique de la nouvelle matiere dans l'emploi du temps
 */
function AjoutMatiere() {
    require("../Model/Edt_Model.php");
    $idModule = $_POST['idModule'];
    $nomMatiere = $_POST['nomMatiere'];
    $couleur = $_POST['couleur'];
    $profs = explode(',',$_POST['profs']);
    $reponse = AjoutMatiereBDD($idModule,$profs,$nomMatiere, $couleur);
    echo json_encode($reponse);
}

?>

<?php

/**
 * Fonction permettant de récuperer les matières depuis la BDD
 */
function getMatieres(){
    require('./Model/Connect_BDD.php');
    $sqlMatiere = "SELECT * FROM matiere ORDER BY id_mod"; 
    $sqlMatierePrepared = $BDD->prepare($sqlMatiere);
    $sqlMatierePrepared->execute();
    $matiere = $sqlMatierePrepared->fetchAll(PDO::FETCH_ASSOC);
    
    return $matiere;
}

/**
 * Fonction permettant de récuperer les "modules" (et pas les ues) depuis la BDD
 */
function getModules(){
    require('./Model/Connect_BDD.php');
    $sqlModule = "SELECT * FROM uemodule WHERE classif LIKE ?"; 
    $sqlModulePrepared = $BDD->prepare($sqlModule);
    $sqlModulePrepared->execute(array("module"));
    $module = $sqlModulePrepared->fetchAll(PDO::FETCH_ASSOC);
    foreach ($module as $key) {
        $tabModules[$key['id_uemod']] = $key;
    }
    return $tabModules;
}

/**
 * Fonction permettant de récuperer les differentes periodes depuis la BDD
 */
function getPeriodes(){
    require('./Model/Connect_BDD.php');
    $sqlPeriode = "SELECT * FROM period"; 
    $sqlPeriodePrepared = $BDD->prepare($sqlPeriode);
    $sqlPeriodePrepared->execute();
    $Periode = $sqlPeriodePrepared->fetchAll(PDO::FETCH_ASSOC);
    return $Periode;
}

/**
 * Fonction permettant de récuperer les periodes de chaque matieres (les heures) depuis la BDD
 * Cela est rangé dans un tableau pour une utilisation plus simple
 */
function getMatieresPeriod(){
    require('./Model/Connect_BDD.php');
    $sqlPeriodeMatiere = "SELECT * FROM period_matiere"; 
    $sqlPeriodeMatierePrepared = $BDD->prepare($sqlPeriodeMatiere);
    $sqlPeriodeMatierePrepared->execute();
    $PeriodeMatiere = $sqlPeriodeMatierePrepared->fetchAll(PDO::FETCH_ASSOC);
    $tabPeriodeMatiere = array();
    foreach ($PeriodeMatiere as $key) {
        $tabPeriodeMatiere[$key['id_matiere']][$key['id_period']] = $key;
    }
    return $tabPeriodeMatiere;
}


/**
 * Fonction permettant de récuperer les labels des professeurs de chaque matieres (ex : "JMI, CK") depuis la BDD
 * Cela est rangé dans un tableau pour une utilisation plus simple
 */
function getMatieresProf(){
    require('./Model/Connect_BDD.php');
    $sqlProfMatiere = "SELECT * FROM prof_matiere PM, prof P WHERE PM.id_prof = P.id_prof"; 
    $sqlProfMatierePrepared = $BDD->prepare($sqlProfMatiere);
    $sqlProfMatierePrepared->execute();
    $ProfMatiere = $sqlProfMatierePrepared->fetchAll(PDO::FETCH_ASSOC);
    $tabProfMatiere = array();
    foreach ($ProfMatiere as $key) {
        if(empty($tabProfMatiere[$key['id_matiere']])){
            $tabProfMatiere[$key['id_matiere']] = $key['label'];
        }else{
            $tabProfMatiere[$key['id_matiere']] = $tabProfMatiere[$key['id_matiere']].', '.$key['label'];
        }
    }
    return $tabProfMatiere;
}

/**
 * Fonction permettant de récuperer les professeurs depuis la BDD
 */
function getProfs(){
    require('./Model/Connect_BDD.php');
    $sqlProf = "SELECT * FROM prof"; 
    $sqlProfPrepared = $BDD->prepare($sqlProf);
    $sqlProfPrepared->execute();
    $Profs = $sqlProfPrepared->fetchAll(PDO::FETCH_ASSOC);
    
    return $Profs;
}

/**
 * Fonction permettant d'enregistrer dans la BDD, chaque modification faites à
 * l'emploi du temps (changement, déplacement et ajout d'horaires)
 * Param $idMatiere : l'identifiant de la matière sur laquelle est effectuée la modification
 * Param $idPeriod : la periode sur laquelle est effectuée la modification
 * Param $nbH : La nouvelle valeur a ajouter dans la BDD
 */
function enregistrerBDD($idMatiere,$idPeriod, $nbH){
    require('../Model/Connect_BDD.php');
    $sqlGetPeriod = "SELECT * FROM period_matiere WHERE id_period = ? AND id_matiere = ?";
    $sqlGetPeriodPrepared = $BDD->prepare($sqlGetPeriod);
    $sqlGetPeriodPrepared->execute(array($idPeriod, $idMatiere));
    $sqlGetPeriodResultat = $sqlGetPeriodPrepared->fetch(PDO::FETCH_ASSOC);

    if($sqlGetPeriodResultat == false){
        $sqlAddPeriod = "INSERT into period_matiere (id_period, id_matiere, nbH) VALUES(?,?,?)";
        $sqlAddPeriodPrepared = $BDD->prepare($sqlAddPeriod);
        $sqlAddPeriodPrepared->execute(array($idPeriod, $idMatiere, $nbH));
    }else{
        $sqlChangePeriod = "UPDATE period_matiere SET nbH = ?  WHERE id_period = ? AND id_matiere = ?";
        $sqlChangePeriodPrepared = $BDD->prepare($sqlChangePeriod);
        $sqlChangePeriodPrepared->execute(array($nbH, $idPeriod, $idMatiere));
    }
}


/**
 * Fonction permettant d'ajouter une matière ainsi que ses professeurs dans la BDD
 * Param $idModule : l'identifiant du module auquelle appartient la nouvelle matière
 * Param $profs : la liste des professeurs
 * Param $nomMatiere : Le nom de la nouvelle matière
 * Param $couleur : La couleur de la nouvelle matière
 * Return L'identifiant de la nouvelle matière, le label des professeurs et la liste des periodes
 */
function AjoutMatiereBDD($idModule,$profs,$nomMatiere, $couleur) {
    require('../Model/Connect_BDD.php');
    $BDD->beginTransaction();
    $sqlAddMatiere = "INSERT into matiere (id_mod, nom, couleur) VALUES(?,?,?)";
    $sqlAddMatierePrepared = $BDD->prepare($sqlAddMatiere);
    $sqlAddMatierePrepared->execute(array($idModule, $nomMatiere, $couleur));
    $id = $BDD->lastInsertId();
    $prof_labels = "";
    foreach ($profs as $idProf ) {
        $sqlProf = "INSERT into prof_matiere (id_prof, id_matiere) VALUES(?,?)";
        $sqlProfPrepared = $BDD->prepare($sqlProf);
        $sqlProfPrepared->execute(array($idProf, $id));

        $sqlProfLabel = "SELECT label FROM prof WHERE id_prof = ?";
        $sqlProfLabelPrepared = $BDD->prepare($sqlProfLabel);
        $sqlProfLabelPrepared->execute(array($idProf));
        $ProfLabel = $sqlProfLabelPrepared->fetch(PDO::FETCH_ASSOC);
        if($prof_labels == ""){
            $prof_labels = $ProfLabel['label'];
        }else{
            $prof_labels .= ", ".$ProfLabel['label'];
        }
        
    }
    

    $BDD->commit();
    $reponse =array();
    $reponse['id'] = $id;
    $reponse['profLabels'] =$prof_labels;
    $sqlPeriode = "SELECT * FROM period"; 
    $sqlPeriodePrepared = $BDD->prepare($sqlPeriode);
    $sqlPeriodePrepared->execute();
    $Periode = $sqlPeriodePrepared->fetchAll(PDO::FETCH_ASSOC);
    $reponse['periodes'] = $Periode;
    return $reponse;
}

?>
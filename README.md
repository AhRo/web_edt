﻿# Installation :

- Ajouter le dossier dans votre dossier WWW
- Ajouter la base de données situé dans le dossier base de données à votre phpmyadmin
- Modifier le fichier Connect_BDD.php present dans Model pour la connexion à la base de données


# Fonctionnalités :

- Ajout de matières dans la BDD + ajout dynamique de la matiére dans l'emploi du temps
- Fonction Ajax  : Enregistrement automatique + Ajout de matière
- Déplacement des horaires
- Déplacement d'une horaire sur une autre horaires implique un changement des deux horaires (switch)
- Calcul des totaux automatique (à chaque changement)
- Poubelle pour la suppression des horaires
- Couleur par matière
- Conservation de la couleur de la matière au changement des horaires
- Affichage des labels des professeurs + horaires en php

# Bibliotheques :

- JQUERY 1.12.4
- JQUERY UI 1.12.1
- BOOTSTRAP 3.4.0
- BOOTSTRAP COLORPICKER 2.5.2
- BOOTSTRAP SELECT 1.13.2


-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 03 Février 2019 à 22:57
-- Version du serveur :  5.7.21-log
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `pweb2`
--

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

CREATE TABLE `matiere` (
  `id_mat` int(4) NOT NULL,
  `id_mod` int(4) NOT NULL,
  `nom` text COLLATE utf8_bin NOT NULL,
  `couleur` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `matiere`
--

INSERT INTO `matiere` (`id_mat`, `id_mod`, `nom`, `couleur`) VALUES
(1, 3, 'Programmation WEB côté Serveur (M3104)', '#1DE124'),
(2, 3, 'Programmation WEB côté Serveur - JAVA (M3104-2)', ''),
(9, 4, 'Algorithmique avancée', ''),
(14, 5, 'Anglais', ''),
(15, 6, 'Expression Communication', ''),
(16, 7, 'Modélisation Objet', ''),
(17, 8, 'PROBA STAT', ''),
(63, 4, 'nouvelle Matiere', '#195ec7'),
(81, 3, 'Nouvelle Matiere', '#164fe0'),
(83, 3, 'qsdqs', '#7a0d1a'),
(89, 6, 'Math', '#2edb2a'),
(90, 3, 'Java', '#0ea341');

-- --------------------------------------------------------

--
-- Structure de la table `period`
--

CREATE TABLE `period` (
  `id_period` int(11) NOT NULL,
  `id_promo` int(11) NOT NULL,
  `label` text NOT NULL,
  `tDeb` int(11) NOT NULL,
  `tFin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `period`
--

INSERT INTO `period` (`id_period`, `id_promo`, `label`, `tDeb`, `tFin`) VALUES
(1, 1, 'C', 1513292400, 1540656000),
(2, 1, 'D', 1540677600, 1541178000);

-- --------------------------------------------------------

--
-- Structure de la table `period_matiere`
--

CREATE TABLE `period_matiere` (
  `id` int(11) NOT NULL,
  `id_period` int(11) NOT NULL,
  `id_matiere` int(11) NOT NULL,
  `nbH` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `period_matiere`
--

INSERT INTO `period_matiere` (`id`, `id_period`, `id_matiere`, `nbH`) VALUES
(1, 1, 1, '30'),
(4, 1, 2, ''),
(5, 1, 9, '15'),
(8, 1, 14, '15'),
(9, 1, 15, ''),
(12, 1, 16, ''),
(14, 1, 17, ''),
(36, 1, 63, '12'),
(32, 1, 81, ''),
(34, 1, 83, '12'),
(38, 1, 89, '13'),
(40, 1, 90, '13'),
(2, 2, 1, '26'),
(3, 2, 2, ''),
(7, 2, 9, ''),
(6, 2, 14, ''),
(10, 2, 15, ''),
(11, 2, 16, '15'),
(13, 2, 17, ''),
(37, 2, 63, ''),
(33, 2, 81, ''),
(35, 2, 83, ''),
(39, 2, 89, ''),
(41, 2, 90, '25');

-- --------------------------------------------------------

--
-- Structure de la table `prof`
--

CREATE TABLE `prof` (
  `id_prof` int(11) NOT NULL,
  `genre` text COLLATE utf8_bin NOT NULL,
  `nom` text COLLATE utf8_bin NOT NULL,
  `prenom` text COLLATE utf8_bin NOT NULL,
  `email` text COLLATE utf8_bin NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL,
  `login_prof` text COLLATE utf8_bin NOT NULL,
  `pass_prof` text COLLATE utf8_bin NOT NULL,
  `date_prof` date NOT NULL,
  `urlPhoto` text COLLATE utf8_bin NOT NULL,
  `couleur` text COLLATE utf8_bin NOT NULL,
  `bConnect` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `prof`
--

INSERT INTO `prof` (`id_prof`, `genre`, `nom`, `prenom`, `email`, `label`, `login_prof`, `pass_prof`, `date_prof`, `urlPhoto`, `couleur`, `bConnect`) VALUES
(1, 'M', 'Ilié', 'Jean-Michel', 'jmilie@parisdescartes.fr', 'JMI', 'jmilie', '1f96593fca60f453f71d8b75f4afc17835769762', '2018-09-12', '', '#E1B36E', 0),
(2, 'M', 'Foughali', 'Karim', 'kfoughali@gmail.com', 'KF', 'kfoughali', 'ecf66de73808530b5e5dc5462cfd074d085fa89c', '2018-01-12', '', '#0002E1', 0),
(3, 'M', 'Kurtz', 'Camille', 'ckurtz@parisdescartes.fr', 'CK', 'ckurtz', 'ed9b1bd745d9a724280d8374184de26b0718960b', '2018-09-09', '', '', 0),
(4, 'M', 'Sortais', 'Michel', 'msortais@parisdescartes.fr', 'MS', 'msortais', '*69358EEA623E244E978C10364CA96398D642E1BE', '2018-09-10', '', '#0002E1', 0),
(5, 'M', 'Ouziri', 'Mourad', 'mouziri@parisdescartes.fr', 'MO', 'mouziri', '*F07FBB4086D1DF19106B375B6E5D7A11BB485C2E', '2018-09-10', '', '#040e00', 0),
(6, 'Mme', 'Dirani', 'Hélène', 'hdirani@parisdescartes.fr', 'HD', 'hdirani', '*81DD599CAA08EA3C67AB17DD86AB3094216B1EC4', '2018-09-10', '', '#fffd00', 0),
(7, 'M', 'Poitrenaud', 'Denis', 'dpoitrenaud', 'DP', 'dpoitrenaud', '*A6C32F3A9310014F2B06BDCCD1752D9C47A73A38', '2018-09-10', '', '', 0),
(8, 'Mme', 'Marechal', 'Laurence', 'lmarechal@parisdescartes.fr', 'LM', 'lmarechal', '*0DFC2E234404E6A97CC9456A8DD457521BF77C97', '2018-09-10', '', '', 0),
(9, 'M', 'Oliviero', 'Philippe', 'poliviero@parisdescartes.fr', 'PhO', 'poliviero', '*2CED6004642B247385E3DEEF9ECF1D4007AC2492', '2018-09-10', '', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `prof_matiere`
--

CREATE TABLE `prof_matiere` (
  `id` int(11) NOT NULL,
  `id_prof` int(11) NOT NULL,
  `id_matiere` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `prof_matiere`
--

INSERT INTO `prof_matiere` (`id`, `id_prof`, `id_matiere`) VALUES
(3, 1, 1),
(4, 2, 1),
(5, 3, 81),
(6, 4, 81),
(8, 1, 83),
(9, 2, 83),
(10, 4, 89),
(11, 3, 90),
(12, 5, 90);

-- --------------------------------------------------------

--
-- Structure de la table `uemodule`
--

CREATE TABLE `uemodule` (
  `id_uemod` int(11) NOT NULL,
  `id_form` int(11) NOT NULL,
  `classif` text COLLATE utf8_bin NOT NULL,
  `nom` text COLLATE utf8_bin NOT NULL,
  `label` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `uemodule`
--

INSERT INTO `uemodule` (`id_uemod`, `id_form`, `classif`, `nom`, `label`) VALUES
(1, 1, 'ue', 'Informatique avancée', '31'),
(2, 1, 'ue', 'Culture scientifique, sociale et humaine avancées', '32'),
(3, 1, 'module', 'Programmation Web côté serveur', '3104'),
(4, 1, 'module', 'Algorithmique avancée', '3103'),
(5, 1, 'module', 'Conception et programmation objet avancées', '3105'),
(6, 1, 'module', 'Probabilités et statistiques', '3201'),
(7, 1, 'module', 'Expression-Communication – Communication\r\nprofessionnelle', '3205'),
(8, 1, 'module', 'Collaborer en anglais', '3206');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`id_mat`),
  ADD KEY `id_mod` (`id_mod`);

--
-- Index pour la table `period`
--
ALTER TABLE `period`
  ADD PRIMARY KEY (`id_period`);

--
-- Index pour la table `period_matiere`
--
ALTER TABLE `period_matiere`
  ADD PRIMARY KEY (`id_period`,`id_matiere`),
  ADD KEY `id_period` (`id_period`),
  ADD KEY `id_matiere` (`id_matiere`),
  ADD KEY `id` (`id`);

--
-- Index pour la table `prof`
--
ALTER TABLE `prof`
  ADD PRIMARY KEY (`id_prof`);

--
-- Index pour la table `prof_matiere`
--
ALTER TABLE `prof_matiere`
  ADD PRIMARY KEY (`id_prof`,`id_matiere`) USING BTREE,
  ADD KEY `id` (`id`),
  ADD KEY `FK_ProfMatiere_Matiere` (`id_matiere`);

--
-- Index pour la table `uemodule`
--
ALTER TABLE `uemodule`
  ADD PRIMARY KEY (`id_uemod`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `matiere`
--
ALTER TABLE `matiere`
  MODIFY `id_mat` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT pour la table `period`
--
ALTER TABLE `period`
  MODIFY `id_period` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `period_matiere`
--
ALTER TABLE `period_matiere`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT pour la table `prof`
--
ALTER TABLE `prof`
  MODIFY `id_prof` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `prof_matiere`
--
ALTER TABLE `prof_matiere`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `uemodule`
--
ALTER TABLE `uemodule`
  MODIFY `id_uemod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD CONSTRAINT `FK_Matier_Module` FOREIGN KEY (`id_mod`) REFERENCES `uemodule` (`id_uemod`);

--
-- Contraintes pour la table `period_matiere`
--
ALTER TABLE `period_matiere`
  ADD CONSTRAINT `FK_PeriodMatiere_Matiere` FOREIGN KEY (`id_matiere`) REFERENCES `matiere` (`id_mat`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_PeriodMatiere_Period` FOREIGN KEY (`id_period`) REFERENCES `period` (`id_period`);

--
-- Contraintes pour la table `prof_matiere`
--
ALTER TABLE `prof_matiere`
  ADD CONSTRAINT `FK_ProfMatiere_Matiere` FOREIGN KEY (`id_matiere`) REFERENCES `matiere` (`id_mat`),
  ADD CONSTRAINT `FK_ProfMatiere_Prof` FOREIGN KEY (`id_prof`) REFERENCES `prof` (`id_prof`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

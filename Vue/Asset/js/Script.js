var triggered = false;
$( function() {

    /**
     * Submit des informations de la modal (création de la matière)
     */
    $('#addMatiere').on('click',function(){
        var idModule = $('#idModule').val();
        var nomMatiere = $('#nomMatiere').val();
        var couleur = $('#couleur').val();
        var profs = $('#idProfs').val();
        if(profs != null && couleur != "" && nomMatiere != "" && idModule != ""){
            GetModulesInSelect(idModule, profs, nomMatiere, couleur);
            $('#ModalMatiere').modal('toggle');
            $('#nomMatiere').val('');
            $('#couleur').val('');
            $('#idProfs').val();
            $('#idProfs').change();
        }
        
    });

    /**
     * Initialisation du colorpicker
     */
    $('#couleur').colorpicker({
        format: 'hex'
    });
    
    /**
     * Trigger permettant d'effactuer le calcul des heures au tous début
     */
    $('.heure').trigger('DOMSubtreeModified')
    triggered = true;
    var drag;
    
    /**
     * Création des draggables
     */
    $( ".heure" ).draggable({
         revert: "invalid",
         revertDuration: 0
         
    });

    /**
     * Création des droppables
     */
    $( ".droppable" ).droppable({
    drop: function( event, ui ) {
        var t = $(this)[0].children[0].innerHTML;
        $(this)[0].children[0].innerHTML = ui.draggable[0].innerHTML;
        ui.draggable[0].innerHTML = t;
        $(ui.draggable).css({ "left": "0", "top": "0" });
        $(this).find("div").focus();
    }
    });

    /**
     * droppable permettant de revert la draggable et de supprimer sa valeur
     */
    $( "#trash" ).droppable({
        drop: function( event, ui ) {
            ui.draggable[0].innerHTML=''
            $(ui.draggable).draggable("option", "revert", true)
        }
        });
    } );

    /**
     * Permet de n'avoir de des nombre pour les heures
     */
    $('.heure').on("keypress", function(e){
        if (!$.isNumeric(e.key))
            e.preventDefault()
    });

    /**
     * Permet d'avoir les draggable au dessus des autres elements
     */
    $('.heure').on("dragstart", function(e){
        $(this).css("cursor", 'grab')

        if($(this).text() == null || ($(this).text() == "")){
            e.preventDefault();
            return
        }
        $(this).css("z-index", '100')
        $(this).css('border','1px solid black')
        
    });

    /**
     * Changement du cursor
     */
    $('.heure').on("mouseover", function(e){
        $(this).css("cursor", 'pointer')
        
    });

    /**
     * retour valeurs initiaux avant dragstart
     */
    $('.heure').on("dragstop", function(e){
        $(this).css("z-index", '0')
        $(this).css("cursor", 'text')
        $(this).css('border','0')
        });

    /** 
     * Listener sur les modifications sur le tableaux
     * Permet de calculer les totaux à chaque modifications
     * + permet d'effecuter un enregistrement AUTOMATIQUE à chaque modification
     * 
     */    
    $("body").on('DOMSubtreeModified', "td>div", function() {
        console.log('dfgd')
        if(triggered != false){
            var idperiod=$(this).parent().data("idperiod");
            var nbH = $(this).text()
            var matiere = $(this).parents().eq(1).data("id")
            enregistrer(matiere, idperiod, nbH);
        }
        
        var i;
        var tab = [];
        var cpt = 0;
        $('.matiere').each(function(){
            i = 0;
            cpt = 0;
            $(this).find(".heure").each(function(){
                if(tab[cpt] == null){
                    tab[cpt] = 0;
                }
                if($(this).text() ==""){
                    var heure = 0;
                }else{
                    var heure = parseInt($(this).text());
                }
                tab[cpt] = parseInt(tab[cpt]) + heure;
                cpt = cpt+1;
                i = i+ heure;

            });
            
            $(this).find('td:last')[0].innerHTML=i;
            var cpt2 = 0;
            var total = 0
            $('.total').each(function(){
                $(this)[0].innerHTML = tab[cpt2];
                
                total = total + tab[cpt2];
                cpt2 = cpt2+1;
            });
            $('tr:last').find('td:last')[0].innerHTML = total
            
        });
    });

/**
 * Fonction utilisant Ajax pour enregistrer dans la BDD chaque modification faites à l'edt
 * @param {*} idMatiere : l'identifiant de la matière sur laquelle est effectuée la modification
 * @param {*} idPeriod  : la periode sur laquelle est effectuée la modification
 * @param {*} nbH : La nouvelle valeur a ajouter dans la BDD
 */
function enregistrer(idMatiere, idPeriod, nbH){
    var tab = new FormData();
    tab.append("action", "enregistrer");
    tab.append("idMatiere", idMatiere);
    tab.append("idPeriod", idPeriod);
    tab.append("nbH", nbH);
    $.ajax({
        url: './Controle/Edt_Controller.php',
        type: 'POST',
        data: tab,
        processData: false,
        contentType: false,
        dataType: "json",
        success: function (res) {
            console.log(res)
                
        },
        error: function (error) {
            console.log(error);
        }
    });
}

/**
 * Fonction utilisant AJAX pour enregistrer la matiere dans la BDD + ajoute une ligne au tableau à lendroit correspondant
 * @param {*} idModule : l'identifiant du module auquelle appartient la nouvelle matière
 * @param {*} profs : la liste des professeurs
 * @param {*} nomMatiere : Le nom de la nouvelle matière
 * @param {*} couleur : La couleur de la nouvelle matière
 */
function GetModulesInSelect(idModule, profs, nomMatiere, couleur){
    var tab = new FormData();
    tab.append("action", "AjoutMatiere");
    tab.append("idModule", idModule);
    tab.append("profs", profs);
    tab.append("nomMatiere", nomMatiere);
    tab.append("couleur", couleur);
    $.ajax({
        url: './Controle/Edt_Controller.php',
        type: 'POST',
        data: tab,
        processData: false,
        contentType: false,
        dataType: "json",
        success: function (res) {
            console.log(res)
            var html = '<tr class ="matiere" data-id='+res.id+' data-idmodule='+idModule+' style="background-color:'+couleur+';"><td>'+nomMatiere+'</td><td>'+res.profLabels+'</td>'
            res.periodes.forEach(element => {
                html+='<td class="droppable" data-idperiod='+element.id_period+' ><div contenteditable class="heure"></div></td>'
            });
            html+='<td>0</td></tr>'
            $('*[data-idmodule='+idModule+']:last').after(html)
            
            $( ".heure" ).draggable({
                revert: "invalid",
                revertDuration: 0
                
           });

           $( ".droppable" ).droppable({
            drop: function( event, ui ) {
                /*console.log(event)
                console.log(ui)
                console.log($(this));
                console.log("children");
                console.log($(ui.draggable).parent())*/
                var t = $(this)[0].children[0].innerHTML;
                //console.log($(this)[0].children)
                $(this)[0].children[0].innerHTML = ui.draggable[0].innerHTML;
                ui.draggable[0].innerHTML = t;
                /*console.log(event)
                console.log(ui)*/
                //ui.draggable.c
                
                $(ui.draggable).css({ "left": "0", "top": "0" });
                $(this).find("div").focus();
            }
            });
        },
        error: function (error) {
            console.log(error);
        }
    });
}
<!doctype html>
<?php
$idMod = 0;
?>
<html>
    <head>
        <meta charset="utf-8">
        <title>Emploi du temps</title>
        <!-- CSS BOOTSTRAP EN LIGNE -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <!-- CSS COLORPICKER (permet de chosiir des couleurs) -->
        <link rel="stylesheet" href="./Vue/Asset/lib/bootstrap-colorpicker-2.5.2/dist/css/bootstrap-colorpicker.css">
        <!-- CSS SELECT (BOOTSTRAP) -->
        <link rel="stylesheet" href="./Vue/Asset/lib/bootstrap-select-1.13.2/dist/css/bootstrap-select.min.css">
        <!-- CSS personalisé -->
        <link rel="stylesheet" href="./Vue/Asset/css/style.css">
        
        
    </head>

    <body>
        <!-- Bouton ajout de matiere -->
    <button id="addLigne" type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalMatiere"> Ajouter une matière </button>
        <!-- Emploi du temps -->
        <table style="width:70%">
            <tr>
                <th >Module / Matiere</th>
                <th>Profs</th> 
                <?php foreach($periodes as $periode): ?>
                <th><?= date("d/m/Y",$periode["tDeb"])." - ".date("d/m/Y",$periode["tFin"])?></th>
                <?php endforeach; ?>
                
                <th>Total</th>
            </tr>
            <?php foreach($matieres as $key): ?>
                <?php if($key["id_mod"] != $idMod): ?>
                    <tr>
                        <td style="background-color:grey;" colspan="5"><?=$modules[$key["id_mod"]]['nom']?></td>
                        
                    </tr>
                    <?php $idMod = $key["id_mod"];?>
                <?php endif; ?>
                <tr class ="matiere" data-id=<?=$key['id_mat']?> data-idModule=<?=$key['id_mod']?> style="background-color:<?= $key['couleur'] != null ? $key['couleur'] : 'white' ?>;">
                    <td><?= $key['nom']?></td>
                    <td><?= !empty($profsMatiere[$key['id_mat']]) ? $profsMatiere[$key['id_mat']] : ""?></td>
                    <?php foreach($periodes as $periode): ?>
                        <?php if(!empty($periodesMatiere[$key['id_mat']][$periode['id_period']])): ?>
                            <td class="droppable" data-idPeriod=<?=$periode['id_period']?>><div contenteditable class="heure"><?=$periodesMatiere[$key['id_mat']][$periode['id_period']]['nbH']?></div></td> 
                        <?php else : ?>
                            <td class="droppable" data-idPeriod=<?=$periode['id_period']?>><div contenteditable class="heure"></div></td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td></td>
                </tr>
            <?php endforeach; ?>

            <tr>
                <td>Total</td>
                <td></td>
                <?php foreach($periodes as $periode): ?>
                    <td class="total"></td>
                <?php endforeach; ?>
                <td></td>
            </tr>
        </table>
        <!-- POUBELLE -->
        <div id="trash"><img src="./Vue/Asset/images/poubelle.png" alt="Poubelle" width="100%"></div>
        <!-- MODAL AJOUT DE MATIERE -->
        <div class="modal fade" id="ModalMatiere" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Ajout d'une matière</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-md-12">Nom du module
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-12">
                        <select class="selectpicker" id="idModule" data-live-search="true">
                            <?php foreach ($modules as $module) :?>
                                <option value=<?=$module['id_uemod']?>><?=$module['nom']?></option>
                            <?php endforeach; ?>
                        </select>

                            <br>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-12">Nom de la matière
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="nomMatiere" id="nomMatiere" />
                            <br>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-12">Couleur de la matière
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-12">
                            <input id="couleur" type="text" class="form-control" value="" />
                            <br>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-12">Professeurs
                            <span class="required"> * </span>
                        </label>
                        <div class="col-md-12">
                            <select class="selectpicker" id="idProfs" data-live-search="true" multiple>
                                <?php foreach ($profs as $prof) :?>
                                    <option value=<?=$prof['id_prof']?>><?=$prof['prenom'].' '.$prof['nom']?></option>
                                <?php endforeach; ?>
                            </select>

                            <br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn btn-primary" id="addMatiere">Ajouter</button>
                </div>
                </div>
            </div>
        </div>
    </body> 
    <!-- JS JQUERY 1.12.4 -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <!-- JS JQUERY UI 1.12.1 -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- JS BOOTSTRAP 3.4.0-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <!-- JS BOOTSTRAP COLORPICKER 2.5.2-->
    <script src="./Vue/Asset/lib/bootstrap-colorpicker-2.5.2/dist/js/bootstrap-colorpicker.js" type="text/javascript"></script>
    <!-- JS BOOTSTRAP SELECT 1.13.2-->
    <script src="./Vue/Asset/lib/bootstrap-select-1.13.2/dist/js/bootstrap-select.min.js" type="text/javascript"></script>
    <!-- JS PERSONNALISE -->
    <script src="./Vue/Asset/js/Script.js" type="text/javascript"></script>
    <script>
     
    </script>

</html>
